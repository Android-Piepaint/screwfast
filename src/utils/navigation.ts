// An array of links for navigation bar
const navBarLinks = [
  { name: "Home", url: "/" },
  { name: "Screenshots", url: "/screenshots" },
  { name: "Contact", url: "/contact"},
  { name: "Manuals", url: "https://linux-on-nabu.gitbook.io/linux-for-mi-pad-5/"},
  { name: "Download", url: "/downloads" }
];
// An array of links for footer
const footerLinks = [
  {
    section: "Resources",
    links: [
      { name: "Documentation", url: "https://linux-on-nabu.gitbook.io/linux-for-mi-pad-5/" },
      { name: "Tools & Firmwares(from Timoxa0's Git repositories)", url: "https://timoxa0.su/?dir=share/nabu/packages" },
      { name: "Git Repositories", url: "/services" },
    ],
  },
];
// An object of links for social icons
const socialLinks = {
  facebook: "https://www.facebook.com/",
  x: "https://twitter.com/",
  github: "https://github.com/mearashadowfax/ScrewFast",
  google: "https://www.google.com/"
};

export default {
  navBarLinks,
  footerLinks,
  socialLinks,
};